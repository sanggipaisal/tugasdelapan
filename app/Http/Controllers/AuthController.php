<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('page.register');
    }

    public function kirimdata(Request $request)
    {
        $namadepan = $request['namadepan'];
        $namabelakang = $request['namabelakang'];

        return view('page.welcome', compact("namadepan","namabelakang"));

    }
}
