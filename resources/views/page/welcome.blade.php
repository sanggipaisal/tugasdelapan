@extends('layout.master')
@section('title')
Halaman Index
@endsection
@section('content')
    <h1>SELAMAT DATANG! {{ $namadepan }} {{ $namabelakang }}</h1>
    <p><b>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</b></p>
@endsection