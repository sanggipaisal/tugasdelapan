@extends('layout.master')
@section('title')
Halaman Form
@endsection
@section('content')
    <h3>Buat Account Baru</h3>
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="POST">
        @csrf
        <label for="namadepan">First name :</label><br>
        <input type="text" name="namadepan"><br><br>
        <label for="namabelakang">Last name :</label><br>
        <input type="text" name="namabelakang"><br><br>
        <label for="jeniskelamin">Gender</label><br>
        <input type="radio" name="jeniskelamin" value="Male">Male<br>
        <input type="radio" name="jeniskelamin"  value="Female">Female<br><br>
        <label for="kewarganegaraan">Nationality</label><br>
        <select name="kewarganegaraan" id="kewarganegaraan">
            <option value="Indonesia">Indonesia</option>
            <option value="Jepang">Jepang</option>
            <Option value="Amerika">Amerika</Option>
        </select><br><br>
        <label for="bahasa">Language Spoken</label><br>
        <input type="checkbox" id="bahasa" name="bahasa1" value="Bahasa Indonesia">Bahasa Indonesia <br>
        <input type="checkbox" id="bahasa" name="bahasa2" value="English">English <br>
        <input type="checkbox" id="bahasa" name="bahasa3" value="Other">Other <br><br>
        <label for="bio">Bio</label> <br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up" >
    </form>  
    @endsection