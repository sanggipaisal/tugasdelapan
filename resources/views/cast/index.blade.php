@extends('layout.master')
@section('title')
Halaman List Pemeran
@endsection
@section('content')

<a href="/cast/create" class="btn btn-primary" mb-3>Tambah Pemeran</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Pemeran</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio (Preview)</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $item->nama}}</td>
            <td>{{ $item->umur}}</td>
            <td>{{ substr($item->bio,0,50)}}... <a href="/cast/{{ $item->id }}">Detail</a></td>
            <td>
                
                <form action="/cast/{{ $item->id }}" method="POST">
                    <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Ubah</a>
                    @csrf
                    @method('delete')
                    <input type="submit" value="Hapus" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
      @empty
        <h1>Data Pemeran masih kosong</h1>         
      @endforelse
    </tbody>
  </table>

@endsection