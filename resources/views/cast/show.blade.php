@extends('layout.master')
@section('title')
Halaman Detail Pemeran
@endsection
@section('content')
<h1>{{ $cast->nama }}</h1>
<h3>Umur : {{ $cast->umur }} Tahun</h3>
<p>{{ $cast->bio }}</p><hr><br>
<a href="/cast" class="btn btn-warning">Kembali</a>
@endsection