@extends('layout.master')
@section('title')
Halaman Edit Pemeran
@endsection
@section('content')
<form method="POST" action="/cast/{{ $cast->id }}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Pemeran</label>
      <input type="text" name="nama" value="{{ $cast->nama }}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="text" name="umur" value="{{ $cast->umur }}"class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Biodata</label>
      <textarea name="bio" cols="30" rows="3" class="form-control">{{ $cast->bio }}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Edit</button>&nbsp;<a href="/cast" class="btn btn-warning">Batal</a>
  </form>

@endsection